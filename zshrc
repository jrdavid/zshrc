## ~/.zshrc
# General environment {{{1
if [ -r "$HOME/etc/sh/environment" ] 
then 
	source "$HOME/etc/sh/environment"
fi

PATH="$HOME/usr/bin:$PATH"

this_host="$(hostname)"

ETCDIR="${ETCDIR:-$HOME/etc}" # in case the environment wasn't found above
zdotdir="${ZDOTDIR:-$ETCDIR/zshrc}"
export ZFTP_BMFILE=$zdotdir/zfbookmarks

VARDIR="$HOME/var/zsh"
test -d "$VARDIR" || mkdir -p "$VARDIR"

# Search for 'linux palette osc' for info on the OSC terminal capability.
case $TERM in
	linux) printf "\e]P44444BB" ;; # Fix dark blue on black in linux console
esac

# Source posixly correct ressources {{{1
if [ -d "$zdotdir/lib" ] ; then
   for file in $zdotdir/lib/*; do
      source $file
   done
   initializeANSI
fi

# Source zsh-specific ressources {{{1
if [ -d "$zdotdir/functions" ] ; then
   export ZLIBDIR=$zdotdir/functions
   fpath=( $ZLIBDIR $fpath )
   for d in $fpath; do
      f=( $d/[^_]*(.N:t) )
      [ "${#f}" -ne 0 ] && autoload -- $f
   done
fi

autoload is-at-least
autoload zmv
autoload -U zfinit && zfinit
autoload -U zed
zmodload -F zsh/stat b:zstat

# Command-line editing, terminal settings & keymaps {{{1

case $TERM in
   urxvt|rxvt-unicode) printf '\33]701;%s\007' "$LANG";;
esac

# Color stderr in red. Stolen from
# http://gentoo-wiki.com/TIP_Advanced_zsh_Completion
#exec 2>>(while read line; do print '\e[91m'${(q)line}'\e[0m' > /dev/tty; done &)

# For the sanity of other people using my shell...
bindkey -M viins '\e[3~' vi-delete-char
bindkey -M viins '\e[1~' beginning-of-line
bindkey -M viins '\e[4~' end-of-line

# Map to anything but don't kill the shell!
bindkey -M viins '^@' list-choices

# Bring back previous command from history
# and place cursor after the command word.
zle -N up-history-add-option
bindkey -M vicmd '\ep'    up-history-add-option
bindkey -M viins '\ep'    up-history-add-option
bindkey -M viins '\ew'    copy-prev-shell-word
bindkey -M viins '^X^U' undo

# Open editor on command line.
zle -N edit-command-line
bindkey -M viins '\ee' edit-command-line

# Accept line and put the output in $keep array
zle -N accept-line-and-keep
bindkey -M viins '\ej' accept-line-and-keep

zle -N paste_clip
bindkey -M viins '^F' paste_clip

# Quote word before cursor on command line.
zle -N quote-last-word
bindkey -M viins '\eq'  quote-last-word

# avoid funny behavior where vi-forward/backward-word don't jump over
# slashes in directory names.
bindkey -v
bindkey -M vicmd 'b'    backward-word
bindkey -M vicmd 'w'    forward-word
bindkey -M vicmd '\C-r' redo
bindkey -M viins '\C-r' undefined-key
bindkey -M vicmd "ga"   what-cursor-position
bindkey -M vicmd ':'    execute-named-cmd
bindkey -M vicmd 'Q'    push-line
bindkey -M viins '^Q'   push-line

WORDCHARS='*?[]~=&;!#$%^{}<>'
# default: *?_-.[]~=/&;!#$%^(){}<>

/bin/stty eof undef
/bin/stty start undef
/bin/stty stop undef

# 'less' {{{1
export LESS="--no-init \
             --quit-if-one-screen \
             --ignore-case \
             --QUIET \
             --search-skip-screen \
             --LONG-PROMPT \
             --line-numbers\
             --RAW-CONTROL-CHARS"

lessopen="$HOME/etc/less/lesspipe"
test -x "$lessopen" && export LESSOPEN="|$lessopen %s"

less_var_dir="$HOME/var/less"
test -d "$less_var_dir" || mkdir -p -- "$less_var_dir"
export LESSHISTFILE="$less_var_dir/history"

lesskey_src="$HOME/etc/less/lesskey.txt"
lesskey_bin="$less_var_dir/lesskey.bin"

if [ -r "$lesskey_src" ]
then
   lesskey -o "$lesskey_bin" -- "$lesskey_src"
   ln -sf "$lesskey_bin" ~/.less
fi

if [ "${LANG#*.}" = "UTF8" -o "${LANG#*.}" = "UTF-8" ] ; then
   export LESSCHARSET="utf-8"
else
   export LESSCHARSET="iso8859"
fi

# Completion {{{1
#
autoload -U compinit
compinit -d "$VARDIR/zcompdump"

compdef -k _lookuptag menu-select '^X^J'

setopt always_to_end NO_list_beep complete_in_word

zstyle ':completion:*' group-name ''
zstyle ':completion:*' verbose yes
zstyle ':completion:*' user-cache on
zstyle ':completion:*' cache-path $zdotdir/cache
zstyle ':completion:*:descriptions' format %B%d%b
zstyle ':completion:*:warnings' format 'No matches: %d'
zstyle ':completion:*:messages' format %d
zstyle ':completion:*:-command-:*:(commands|builtins|reserved-words|aliases)' \
       group-name commands

zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:(cvs-add|less|rm|vi):*' ignore-line true   
zstyle ':completion:*:*:(cd|jump_to):*' ignored-patterns '(*/|)(CVS)'

zmodload -i zsh/complist
zstyle ':completion:*:default' list-prompt '%S%M matches%s'
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:default'   menu 'select=5'
zstyle ':completion:*:processes' menu 'select=5'
zstyle ':completion:::::' completer _expand _complete _ignored
bindkey -M listscroll q send-break
bindkey -M menuselect '\C-o' accept-and-menu-complete
bindkey -M menuselect 'j'    down-line-or-history
bindkey -M menuselect 'k'    up-line-or-history
bindkey -M menuselect 'h'    backward-char
bindkey -M menuselect 'l'    forward-char
bindkey -M menuselect '\C-z' undo
bindkey '\C-i' complete-word

autoload -U history-beginning-search-menu-end
zle -N history-beginning-search-menu-end
bindkey '\eP' history-beginning-search-menu-end

if [ -r "$HOME/etc/scripts.conf" ]
then
	. "$HOME/etc/scripts.conf"
	compdef "_files -W $devel_dir" shvi shcp shdo
	compdef "_files -W $HOME/download" unpack
fi

zstyle ':completion:*:*:eog:*' file-patterns '*.png:image-files' '%p:all-files'
# Globbing {{{1
setopt csh_null_glob extended_glob
# Directory Navigation {{{1
setopt auto_pushd pushd_ignore_dups
dirstack=(10)
zle -N up-dir
zle -N dir-search-up
bindkey -M vicmd '\Ck' up-dir
bindkey -M viins '\Ck' up-dir
bindkey -M vicmd '?'   dir-search-up
cdpath=( ~ ~/work ~/etc )

hash -d stow=/usr/local/stow
hash -d doc=/usr/share/doc
# Directory marks {{{2
MARKS_FILE=$HOME/var/zsh/marks
typeset -A dir_marks
# load marks
if [ -r $MARKS_FILE ] ; then
   while read key value ; do
      dir_marks[$key]=$value
   done < $MARKS_FILE
fi

usage() {
}

marks() { 
   if [ $# -eq 0 ] ; then
      for k in ${(k)dir_marks}; do
         print -r "$k ${(vq)dir_marks[$k]}"
      done
   fi

   while getopts "hld:gs" arg
   do
      case "$arg" in
         l) # Load marks file 
            if [ -r $MARKS_FILE ] ; then
               while read key value ; do
                  dir_marks[$key]=$value
               done < $MARKS_FILE
               return 0
            else
               print -u2 "$MARKS_FILE doesn\'t exist."
               return 1
            fi
            ;;
         d) # Delete marks
            if [ $OPTARG = "ALL" ] ; then
               set -A dir_marks
               rm -f $MARKS_FILE
            elif [ $dir_marks[$OPTARG] ] ; then
               unset "dir_marks[$OPTARG]"
            else
               print -u2 "$0: No such mark \'$OPTARG\'"
            fi
            return 0
            ;;
         g) # Go to mark, let cd handle errors
            builtin cd $dir_marks[$2]
            return 0
            ;;
         s) # Set mark
            if [ $2 -a -z $3 ] ; then
               dir_marks[$2]=${PWD}
            elif [ $2 -a $3 ]  ; then
               dir_marks[$2]=$3
            else
               print -u2 "which mark?"
               return 1
            fi
            for k in ${(k)dir_marks}; do
               print -r "$k ${(vq)dir_marks[$k]}"
            done >| $MARKS_FILE
            ;;

         : ) ;;
      esac
   done
}

zle -N set-dir-mark
zle -N goto-dir-mark
bindkey -M vicmd 'm'   set-dir-mark
bindkey -M vicmd \;    goto-dir-mark

# Directory jumplist {{{2
set -A jumplist
integer jumppos=1

JUMPLIST_SIZE=50
JUMPS_FILE=$HOME/var/zsh/jumps
[[ -f $JUMPS_FILE ]] && . $JUMPS_FILE
trap '
   print -r "jumplist=(${(@qq)jumplist})" >| $JUMPS_FILE
' EXIT

_pushd() {
   integer i;
   integer len;

   len=${#jumplist}
   if [ $len -eq $JUMPLIST_SIZE ] ; 
   then 
      len=$JUMPLIST_SIZE-1
   fi

   if [ $len -eq 0 ]
   then
      jumplist[1]=${PWD}
   else
      for ((i=$len; i>0; i--))
      do
         jumplist[(($i+1))]=$jumplist[$i]
      done
      jumplist[1]=${PWD}
   fi
}

_popd() {
   local i;

   [ ${#jumplist} -eq 0 ]        && return 1;
   for ((i=1; i<${#jumplist}; i++))
   do
      jumplist[$i]=$jumplist[(($i+1))]
   done
   jumplist[$i]=()
   return 0;
}

jump_to() {
   local i;

   builtin cd $1
   [ $? -eq 0 ] || return 1;
   if [ $jumppos -gt 1 ] ; then
      for ((i=$jumppos; i>1; i--))
      do
         _popd;
      done
      jumppos=1
   fi
   _pushd
}

jumps() {
   integer i

   # With no argument, print jump list
   if [ $# -eq 0 ] ; then
      for ((i=${#jumplist}; i>0; i--))
      do
         if [ $i -eq $jumppos ] 
         then
            print "$(($i-1)) > $jumplist[$i]"
         else
            print "$(($i-1))   $jumplist[$i]"
         fi
      done
      return 0
   fi

   while getopts "d" arg
   do
      case "$arg" in
         d ) set -A jumplist
             return 0
             ;;
         ? ) ;;
      esac
   done
}

zle -N jump_back
zle -N jump_forward
bindkey -M vicmd '\Co' jump_back
bindkey -M viins '\Co' jump_back
# Can't bind c-i in viins because it's too useful for completion
# (i.e. 'tab')
bindkey -M vicmd '\Ci' jump_forward
alias cd=jump_to
compdef jump_to=cd
alias ju=jumps
# History {{{1
HISTFILE="${HISTFILE:-$HOME/var/zsh/history}"
HISTSIZE=500
SAVEHIST=500
setopt hist_verify hist_ignore_all_dups extended_history
bindkey -M viins '\C-P'   history-search-backward
bindkey -M viins '\C-N'   history-search-forward
bindkey -M viins '^Xr'  history-incremental-search-backward
bindkey -M vicmd '^Xr'  history-incremental-search-backward
bindkey -M viins '^R'   accept-line-and-down-history

alias h='fc -liD -10'
alias hi='fc -liD -50'
alias his='fc -liD -100'
H () { fc -liD 1 | grep $* ; }
commit-to-history() {
   print -s ${(z)BUFFER}
   zle send-break
}
zle -N commit-to-history
bindkey "^X^H" commit-to-history

# Prompt and title bar {{{1
autoload -U promptinit
promptinit

setopt prompt_subst

# Setting the prompt is in the host specific file zshrc.$this_host
if [ -r "$zdotdir/prompt.$this_host" ]
then
   source "$zdotdir/prompt.$this_host"
elif [ -r "$zdotdir/prompt.generic" ]
then
   source "$zdotdir/prompt.generic"
fi

titlebar() {
    print -nP "\e]2;$*\a"
}
chpwd () {
	[ "$DISPLAY" ] &&  titlebar "%n@%m: %~"
}

if [ $DISPLAY ] ; then
   titlebar "$TERM - %n@%m - %~"
fi

# Aliases and options for frequently used commands {{{1

alias :q='exit' # :-)
alias :qa='exit' # :-)
alias :qw='exit' # :-)
alias mkdir='nocorrect mkdir'
alias cuts='cut -d\  '
alias sshconf="$EDITOR $HOME/.ssh/config"
alias datestamp="date +'%Y-%m-%d'"
alias timestamp="date +'%Y-%m-%d_%H-%M-%S'"
alias mybc=scriptbc
alias vbm="VBoxManage"
alias tm="tmuxinator"

alias ip='ip -c'
alias ips='ip -br -c a'

# Git shortcuts
alias -g hd=HEAD
alias -g HEAd=HEAD
alias gf="git fetch origin"
alias gs="git status"
alias ta="tig --all"
alias mra="mr -d $HOME fetch"

alias ls='ls --color=auto -w80'
alias ltr='ls -ltr'
# I never use those
#alias man='man -C ~/man/man.conf'
#alias lad='ls -d .*(/)'              # only show dot-directories
#alias lsd='ls -d *(/)'               # only show directories
#alias lsbig="ls -flh *(.OL[1,10])"   # show 10 biggest files
#alias lsnew="ls -rl *(D.om[1,10])"   # show 10 newest files
#alias lsold="ls -rtlh *(D.Om[1,10])" # show 10 oldest files

# Stolen from Christian 'strcat' Schneider
#alias rw="chmod 600"
#alias rwx="chmod 700"
#alias x="chmod +x"
#alias zcp='zmv -C'
#alias zln='zmv -L'
#alias -g  N="&>/dev/null"
#alias -g 1N="1>/dev/null"
#alias -g 2N="2>/dev/null"
#alias -g swapstd12="3>&1 1>&2 2>&3 3>&-"

running-in-tmux() {
   test "$TMUX" && return 0
   return 1
}

running-in-screen() {
   test "$STY" && return 0
   return 1
}
# TODO: combine the following two.

run-screen() {
	SCREENRC="$HOME/etc/screen/screenrc.tmp"
	cp ~/etc/screen/screenrc "$SCREENRC"
	sh ~/etc/screen/hardstatus >> "$SCREENRC" && \
	/usr/bin/screen -c "$SCREENRC"
}

screenrc() {
   cd "$ETCDIR/screenrc"
   if running-in-screen ; then
      screen -t screenrc $EDITOR screenrc
   else
      $EDITOR screenrc
   fi
   cd -
}
zshrc() {
   pushd "$ETCDIR/zshrc"
   if running-in-tmux ; then
       if [ -f "zshrc.$this_host" ]; then
          tmux new-window -n zshrc "$EDITOR -c 'map ZZ :qa<cr>' -O zshrc 'zshrc.$this_host'"
       else
          tmux new-window -n zshrc "$EDITOR zshrc"
      fi
   else
      printf "no tmux\n"
      "$EDITOR" -c 'map ZZ :qa<cr>' -O zshrc "zshrc.$this_host"
   fi
   printf "Install new configuration? [y/N] "
   read REPLY
   case "$REPLY" in
      y*|Y*) make install
         printf "New configuration installed.\n"
         printf "You will need to source it manually for\n"
         printf "changes to take effect. I would to it\n"
         printf "for you but I can't since I'm running\n"
         printf "in a subshell.\n"
         printf "\n"
         ;;
      *) printf "Very well then.\n"
         ;;
   esac
   popd > /dev/null
}

zsh-test() {
   saved_zdotdir="$zdotdir"
   screen -X setenv ZDOTDIR "$ETCDIR/zshrc"
   screen -t test-zsh zsh
   screen -X setenv ZDOTDIR "$saved_zdotdir"
}

alias z=zshrc
alias Z=". $zdotdir/zshrc"
alias e="$EDITOR $ETCDIR/sh/environment"
alias muttrc="$EDITOR $ETCDIR/mutt/muttrc"
alias procmailrc="$EDITOR $ETCDIR/procmail/procmailrc"

alias lupdatedb="updatedb -U $PWD --output=$PWD"
alias llocate="locate --database=$PWD"
alias lglimpseindex="glimpseindex -b -B -H \$PWD \$PWD"
lglimpse() {
   # Search for index file upwards in directory tree.
   index_dir=$PWD
   while [ ! -f $index_dir/.glimpse_index ]
   do
      if [ "$index_dir" = "/" ] ; then
         printf "No index found. Exiting.\n" 1>&2
         return 1
      fi
      index_dir=$(dirname $index_dir)
   done
   glimpse -nyH "$index_dir" $*
}

alias ecc='ecosconfig'
alias fortune='fortune 10% $HOME/fortunes/personal $HOME/fortunes/system'

# Some diff utilities
function mdiff() { diff -udrP "$1" "$2" > diff.`date "+%Y-%m-%d"`."$1" }
function udiff() { diff -urd $* | egrep -v "^Only in |^Binary files " }
function cdiff() { diff -crd $* | egrep -v "^Only in |^Binary files " }

vimhelp() { vim -c "help $1" -c on }

keep() {
   #setopt localoptions nomarkdirs nonomatch nocshnullglob nullglob
   kept=()
   if [[ ! -t 0 ]]; then
      local line
      while read line; do
         kept+=( $line )
      done
   else
      kept=($~*)
   fi
   print -Rl - $kept
}
autoload -U keep-select-menu
zle -N keep-select-menu
bindkey -M viins '\es' keep-select-menu

# Git related keybindings {{{1
zle -N git-status git-status
bindkey '\C-g\C-g' git-status
zle -N git-log git-log
bindkey '\C-g\C-l' git-log
zle -N git-show-branch git-show-branch
bindkey '\C-g\C-b' git-show-branch
zle -N git-fetch git-fetch
bindkey '\C-g\C-f' git-fetch

alias gm='git submodule'

# Show date when each branch was last modified
git-last-update() {
    git for-each-ref --format '%(authordate:iso8601) %(refname)' refs/remotes/origin | sort
}

# Documentation {{{1
alias run-help > /dev/null && unalias run-help
autoload -U run-help
# the default binding to '^[h' is extremely annoying because
# I often type that sequence to exit insert mode and move the
# cursor left.
bindkey -M viins '^[H'  run-help
HELPDIR=$zdotdir/help

function H-glob() {
   echo -e "Glob qualifiers:
   /  directories
   .  plain files
   @  symbolic links
   p  named pipes (FIFOS)
   *  executable plain files
   %  device files (character or block special)
   %b block special files
   %c character special files
   r  owner-readable files (0400)
   w  owner-writable files (0200)
   x  owner-executable files (0100)
   A  group-readable files (0040)
   I  group-writable files (0020)
   E  group-executable files (0010)
   R  world-readable files (0004)
   W  world-writable files (0002)
   X  world-executable files (0001)
   s  setuid files (04000)
   S  setgid files (02000)
   t  files with the sticky bit (01000)
   L  length (Lk+100 -> files larger than 100k) also m for megabytes
   m  modification time. range: (m-4m+1) modified less than 4 but more than 1 day ago
   a  access time
   c  creation time
   u:user: name of the owner
   fSPEC files with access rights matching SPEC e.g. (f:u+rx:)"
   echo -e "Sorting
            oC sort: n name (default)
            L file size
            l number of links
            a access time
            m modification time
            c creation time
            d subdirectory content before current directory
   OC like oC but reverse order"
   echo -e "Glob flags '(#X)' before pattern
   i  case insensitive
   l  lowercase are case insensitive, uppercase match themselves
   I  Case sensitive: locally negates the effect of i or l from that point on."
   echo -e "Glob-related shell options
   EXTENDED_GLOB
   GLOB_DOTS"
   echo -e "Changing what is displayed
   T  appends a trailing qualifier mark to the filenames"
}

#man() {
#   [[ $# -eq 0 ]] && return 1
#   view -c "Man $*" -c "silent! only"
#}
#
#info() {
#   [[ $# -eq 0 ]] && return 1
#   view -c "Man $1.i" -c "silent! only"
#}
#
#perldoc() {
#   [[ $# -eq 0 ]] && return 1
#   view -c "Man $1.pl" -c "silent! only"
#}

# Job control {{{1
setopt NO_hup

# Security {{{1
setopt NO_clobber hist_allow_clobber

# Spelling correction {{{1
setopt correct

# Miscellaneous {{{1
setopt braceccl
setopt NO_beep

# Source host-specific configuration {{{1
[ -r "$zdotdir/zshrc.$this_host" ] && source "$zdotdir/zshrc.$this_host"

# Commands to run {{{1
#echo -e "${pc1}This is ZSH ${pc2}${ZSH_VERSION%.*}${reset}"
#
# Finish on a good note :-)
# (avoids returning false if host-specific file above doesn't exist)
return 0

# vim: set fdm=marker: set ft=sh:
