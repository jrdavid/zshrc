# ~/.zprofile - Initialization file execute by zsh(1)
# for login shells.
#hidden_files=`find . -maxdepth 1 -type f -name ".*" | wc -l`
#hidden_directories=`find . -maxdepth 1 -type d -name ".?*" | wc -l`
#printf "%d hidden files\n" $hidden_files
#printf "%d hidden directories\n" $hidden_directories
[[ -r etc/zsh/zprofile.$HOST ]] && source etc/zsh/zprofile.$HOST
#  export PYTHONPATH=/usr/local/lib/python

# If user connects with agent forwarding, we're done.
if [ -S "$SSH_AUTH_SOCK" ]
then
   printf "Using agent socket at '$SSH_AUTH_SOCK'\n"
else
   GPG_AGENT_INFO_FILE="$HOME/.gpg-agent-info"
   start_gpg_agent="false"
   # In the case of an X login, this belongs in ~/.xsession
   if [ -z "$DISPLAY" -a "$(which gpg-agent)" ] ; then
      if [ -f "$GPG_AGENT_INFO_FILE" ] ; then
         # Check that process is really running
         gpg_agent_pid=$(gawk -F: '/GPG_AGENT_INFO/ { print $2; }' "$GPG_AGENT_INFO_FILE")
         if kill -0 "$gpg_agent_pid" 2> /dev/null ; then
            eval "$(cat $GPG_AGENT_INFO_FILE)"
         else
            start_gpg_agent="true"
         fi
      else
         start_gpg_agent="true"
      fi
   fi

   if $start_gpg_agent ; then
      printf "Starting gpg-agent.\n"
      eval $(gpg-agent --daemon --enable-ssh-support \
         --write-env-file "$GPG_AGENT_INFO_FILE")
   else
      printf "Not starting gpg-agent.\n"
   fi

fi

# vim: ft=sh
