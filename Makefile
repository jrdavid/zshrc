cwd = $(shell pwd)

install:
	mkdir -p $(HOME)/var/zsh
	ln -sf $(cwd)/zshenv    $(HOME)/.zshenv
	ln -sf $(cwd)/zshrc    $(HOME)/.zshrc
	ln -sf $(cwd)/zprofile $(HOME)/.zprofile
	ln -sf $(cwd)/zlogin $(HOME)/.zlogin
	ln -sf $(cwd)/zlogout  $(HOME)/.zlogout
